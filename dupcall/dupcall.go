// +build !solution

package dupcall

import (
	"context"
	"sync"
)

type Call struct {
	numWaiters int
	mu         sync.Mutex
	once       sync.Once
	broadcast  chan struct{}
	res        interface{}
	err        error
	cancel     context.CancelFunc
	hasValue   bool
}

func (o *Call) Do(
	ctx context.Context,
	cb func(context.Context) (interface{}, error),
) (res interface{}, err error) {
	// Lazy init
	o.once.Do(func() {
		o.numWaiters = 0
		o.broadcast = make(chan struct{}, 10)
	})

	o.mu.Lock()

	if o.numWaiters == 0 {
		// Launch
		newCtx, cancel := context.WithCancel(ctx)
		o.cancel = cancel
		defer cancel()

		go func() {
			r, e := cb(newCtx)
			o.mu.Lock()
			o.res = r
			o.err = e

			numWaiters := o.numWaiters
			for i := 0; i < numWaiters; i++ {
				o.broadcast <- struct{}{}
			}

			o.mu.Unlock()
		}()
	}

	o.numWaiters++
	o.mu.Unlock()

	select {
	case <-ctx.Done():
		o.mu.Lock()

		if o.numWaiters == 1 {
			o.cancel()
		}

		o.numWaiters--

		o.mu.Unlock()

		res, err = nil, ctx.Err()
	case <-o.broadcast:
		res, err = o.res, o.err
		o.mu.Lock()
		o.numWaiters--
		o.mu.Unlock()
	}

	return
}
