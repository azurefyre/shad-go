// +build !solution

package otp

import (
	"fmt"
	"io"
)

type CypherReader struct {
	r    io.Reader
	prng io.Reader
	done bool
}

func Log(s interface{}) {
	// fmt.Println(s)
}

func LogError(s ...interface{}) {
	fmt.Println(s...)
}

func (c *CypherReader) Read(p []byte) (int, error) {
	if c.done {
		return 0, io.EOF
	}

	// return 0, io.EOF

	pLen := len(p)
	ciphertext := make([]byte, pLen)

	Log("Reading ciphertext")
	clen, e := c.r.Read(ciphertext)
	if e != nil {
		LogError("CIPH:", e, clen)
		pLen = clen
		if clen == 0 {
			if e == io.EOF {
				c.done = true
				return 0, io.EOF
			}

			return 0, e
		}
	}

	if clen < pLen {
		pLen = clen
		LogError("clen < pLen")
	}

	decipher := make([]byte, pLen)

	Log("Reading decipher")
	xlen, e := c.prng.Read(decipher)

	if e != nil {
		LogError("DEC: ", e, xlen)
		if e == io.EOF {
			panic("EOF from decipher")
		}

		if xlen < pLen {
			panic("WTF")
		}
	}

	if xlen < pLen {
		panic("xlen < pLen")
	}

	Log("Deciphering")
	for i := 0; i < pLen; i++ {
		p[i] = ciphertext[i] ^ decipher[i]
	}

	return pLen, e
}

type CypherWriter struct {
	w    io.Writer
	prng io.Reader
}

func (w *CypherWriter) Write(p []byte) (n int, err error) {
	pLen := len(p)
	cipher := make([]byte, pLen)

	_, e := w.prng.Read(cipher)
	if e != nil {
		n = 0
		err = e
		return
	}

	ciphertext := make([]byte, pLen)

	for i, b := range p {
		d := cipher[i]
		ciphertext[i] = b ^ d
	}

	wrote, e := w.w.Write(ciphertext)

	err = nil
	if e != nil {
		err = e
	}

	n = wrote
	return
}

func NewReader(r io.Reader, prng io.Reader) io.Reader {
	return &CypherReader{r, prng, false}
}

func NewWriter(w io.Writer, prng io.Reader) io.Writer {
	return &CypherWriter{w, prng}
}
