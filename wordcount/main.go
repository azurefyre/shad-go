// +build !solution

package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

var counter map[string]int

func IncrementCounter(word string) {
	counter[word]++
}

func Check(e error) {
	if e != nil {
		panic(e)
	}
}

func DigestFile(filename string) {
	contents, error := ioutil.ReadFile(filename)
	Check(error)

	for _, word := range strings.Split(string(contents), "\n") {
		IncrementCounter(word)
	}
}

func Report() {
	for k, v := range counter {
		if v > 1 {
			fmt.Printf("%v\t%v\n", v, k)
		}
	}
}

func main() {
	if len(os.Args) <= 1 {
		os.Exit(0)
	}

	counter = make(map[string]int)

	filenames := os.Args[1:]
	for _, file := range filenames {
		DigestFile(file)
	}

	Report()
}
