// +build !solution

package structtags

import (
	"fmt"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"sync"
)

var tagCache sync.Map
var valueCache sync.Map
var tagsDiscovered sync.Map

func DiscoverTagsAndCache(ptr interface{}) (reflect.Value, map[string]reflect.Value) {
	if v, ok := tagsDiscovered.Load(ptr); ok {
		fields, ok2 := tagCache.Load(ptr)
		if !ok2 {
			panic("found in tagsDiscovered but not in tagCache")
		}
		return v.(reflect.Value), fields.(map[string]reflect.Value)
	}

	v := reflect.ValueOf(ptr).Elem()
	fields := make(map[string]reflect.Value)

	for i := 0; i < v.NumField(); i++ {
		fieldInfo := v.Type().Field(i)
		tag := fieldInfo.Tag
		name := tag.Get("http")
		if name == "" {
			name = strings.ToLower(fieldInfo.Name)
		}
		fields[name] = v.Field(i)
	}

	tagsDiscovered.Store(ptr, v)
	tagCache.Store(ptr, fields)

	return v, fields
}

func FieldByTag(v reflect.Value, name string, structname string) (reflect.Value, bool) {
	if tf, ok := tagCache.Load(structname + ":" + name); ok {
		fieldData := tf.(reflect.StructField)
		fieldIndex := fieldData.Index

		return v.FieldByIndex(fieldIndex), true
	}

	return reflect.Value{}, false
}

func Unpack(req *http.Request, ptr interface{}) error {
	if err := req.ParseForm(); err != nil {
		return err
	}

	_, fields := DiscoverTagsAndCache(ptr)

	for name, values := range req.Form {
		f, ok := fields[name]
		if !ok {
			continue
		}

		for _, value := range values {
			if f.Kind() == reflect.Slice {
				elem := reflect.New(f.Type().Elem()).Elem()
				if err := populate(elem, value); err != nil {
					return fmt.Errorf("%s: %v", name, err)
				}
				f.Set(reflect.Append(f, elem))
			} else {
				if err := populate(f, value); err != nil {
					return fmt.Errorf("%s: %v", name, err)
				}
			}
		}
	}
	return nil
}

func populate(v reflect.Value, value string) error {
	switch v.Kind() {
	case reflect.String:
		v.SetString(value)

	case reflect.Int:
		i, err := strconv.ParseInt(value, 10, 64)
		if err != nil {
			return err
		}
		v.SetInt(i)

	case reflect.Bool:
		b, err := strconv.ParseBool(value)
		if err != nil {
			return err
		}
		v.SetBool(b)

	default:
		return fmt.Errorf("unsupported kind %s", v.Type())
	}
	return nil
}
