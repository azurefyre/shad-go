// +build !solution

package varfmt

import (
	"fmt"
	"strconv"
	"strings"
	"unicode/utf8"
)

func FindFormatters(format string) []int {
	answer := make([]int, len(format))
	var lastFormatter rune = ' '

	j := 0

	for i, step := 0, 0; i < len(format); i += step {
		r, l := utf8.DecodeRuneInString(format[i:])
		step = l

		if r == '{' || r == '}' {
			if lastFormatter == r {
				panic("Wrong format: unfinished bracket")
			}

			answer[j] = i
			j++
		}
	}

	if j%2 != 0 {
		panic("Wrong format: uneven number of formatters")
	}

	return answer[:j]
}

func GetArgIndex(format *string, l, r int, formatterIndex int) int {
	if r <= l {
		panic("Error while parsing format string")
	} else if r == l+1 {
		return formatterIndex
	}

	res, err := strconv.Atoi((*format)[l+1 : r])
	if err != nil {
		panic("Couldn't parse number in brackets")
	}

	return res
}

func Sprintf(format string, args ...interface{}) string {
	var sb strings.Builder
	sb.Grow(len(format))

	argCache := make(map[int]string)

	formatters := FindFormatters(format)
	prevR := 0

	for i := 0; i < len(formatters); i += 2 {
		l, r := formatters[i], formatters[i+1]

		// Write slice between formatters
		sb.WriteString(format[prevR:l])

		argIndex := GetArgIndex(&format, l, r, i/2)

		var valueToInsert string

		if cachedValue, ok := argCache[argIndex]; ok {
			valueToInsert = cachedValue
		} else {
			valueToInsert = fmt.Sprint(args[argIndex])
			argCache[argIndex] = valueToInsert
		}

		// Paste argument
		sb.WriteString(valueToInsert)

		prevR = r + 1
	}

	sb.WriteString(format[prevR:])

	return sb.String()
}
