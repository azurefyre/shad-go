// +build !solution

package ratelimit

import (
	"context"
	"errors"
	"time"
)

// Limiter is precise rate limiter with context support.
type Limiter struct {
	lease    chan int
	log      chan time.Time
	isDone   bool
	done     chan int
	done2    chan int
	interval time.Duration
}

var ErrStopped = errors.New("limiter stopped")

// NewLimiter returns limiter that throttles rate of successful Acquire() calls
// to maxSize events at any given interval.
func NewLimiter(maxCount int, interval time.Duration) *Limiter {
	lease := make(chan int, maxCount)
	log := make(chan time.Time, maxCount)
	done := make(chan int, 1)
	done2 := make(chan int, 1)

	for i := 0; i < maxCount; i++ {
		lease <- 1
	}

	go func() {
		var emptyTimer *time.Timer = time.NewTimer(500 * time.Hour)
		var timer *time.Timer = emptyTimer
		waiting := false

		for {
			select {
			case <-done:
				// Cancel
				return
			case firstLease := <-log:
				if !waiting {
					now := time.Now()
					if !firstLease.Add(interval).Before(now) {
						timer = time.NewTimer(firstLease.Add(interval).Sub(now))
						waiting = true
					} else {
						lease <- 1
					}
				}
			}

			if waiting {
				select {
				case <-timer.C:
					lease <- 1
					waiting = false
					timer = emptyTimer
				case <-done:
					// Cancel
					return
				}
			}
		}
	}()

	return &Limiter{lease, log, false, done, done2, interval}
}

func (l *Limiter) Acquire(ctx context.Context) error {
	if l.interval == 0 {
		return nil
	}

	if l.isDone {
		return ErrStopped
	}

	select {
	case <-l.lease:
		// We got the lease
		l.log <- time.Now()

		return nil
	case <-ctx.Done():
		// Cancel
		return ctx.Err()
	case <-l.done:
		// Cancel too
		return nil
	}
}

func (l *Limiter) Stop() {
	l.isDone = true
	close(l.done)
}
