// +build !solution

package hotelbusiness

import (
	"sort"
)

type Guest struct {
	CheckInDate  int
	CheckOutDate int
}

type Load struct {
	StartDate  int
	GuestCount int
}

type Event struct {
	Date int
	Type int
}

////////////////////////////////////////
// Sorting events

type Timeline []Event

func (t Timeline) Len() int           { return len(t) }
func (t Timeline) Swap(i, j int)      { temp := t[i]; t[i] = t[j]; t[j] = temp }
func (t Timeline) Less(i, j int) bool { return t[i].Date < t[j].Date }

////////////////////////////////////////
// Use timelines to solve the problem

func CreateTimeline(guests []Guest) Timeline {
	events := make([]Event, 0)

	if len(guests) == 0 {
		return events
	}

	for _, guest := range guests {
		events = append(events, Event{guest.CheckInDate, 0})
		events = append(events, Event{guest.CheckOutDate, 1})
	}

	sort.Sort(Timeline(events))

	return events
}

func SkipDuplicates(load []Load) []Load {
	var prevGuests int = 0

	withoutDuplicates := []Load{}

	for _, elem := range load {
		if elem.GuestCount != prevGuests {
			withoutDuplicates = append(withoutDuplicates, Load{elem.StartDate, elem.GuestCount})
			prevGuests = elem.GuestCount
		}
	}

	return withoutDuplicates
}

func TimelineToLoad(events []Event) []Load {
	var currentGuests int = 0

	answer := []Load{}
	if len(events) == 0 {
		return answer
	}

	var prevDate int = events[0].Date

	for _, event := range events {
		if prevDate != event.Date {
			answer = append(answer, Load{prevDate, currentGuests})
		}

		if event.Type == 0 {
			currentGuests++
		} else if event.Type == 1 {
			currentGuests--
		} else {
			panic("Wrong event type")
		}

		prevDate = event.Date
	}
	answer = append(answer, Load{prevDate, currentGuests})

	return SkipDuplicates(answer)
}

func ComputeLoad(guests []Guest) []Load {
	timeline := CreateTimeline(guests)
	return TimelineToLoad(timeline)
}
