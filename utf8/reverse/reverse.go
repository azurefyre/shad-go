// +build !solution

package reverse

import (
	"strings"
	"unicode"
	"unicode/utf8"
)

func Reverse(input string) string {
	var sb strings.Builder
	sb.Grow(len(input))

	for i, step := len(input), 0; i > 0; i -= step {
		curRune, w := utf8.DecodeLastRuneInString(input[:i])
		step = w

		if curRune == utf8.RuneError && w == 1 {
			curRune = unicode.ReplacementChar
		}

		sb.WriteRune(curRune)
	}

	return sb.String()
}
