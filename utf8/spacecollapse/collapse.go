// +build !solution

package spacecollapse

import (
	"strings"
	"unicode"
	"unicode/utf8"
)

func CollapseSpaces(input string) string {
	var sb strings.Builder
	sb.Grow(len(input))
	precedingSpace := false

	for i, step := 0, 0; i < len(input); i += step {
		curRune, w := utf8.DecodeRuneInString(input[i:])
		step = w

		if unicode.IsSpace(curRune) {
			if precedingSpace {
				continue
			}
			precedingSpace = true
			curRune = ' '
		} else {
			precedingSpace = false
		}

		// Replace to specific symbol
		if curRune == utf8.RuneError && w == 1 {
			curRune = unicode.ReplacementChar
		}

		sb.WriteRune(curRune)
	}

	return sb.String()
}
