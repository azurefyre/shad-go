// +build !solution

package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"text/tabwriter"
)

var (
	ErrEmptyFile error = fmt.Errorf("empty file")
)

///////////////////////////////////////////////////////////

type FameTable struct {
	commits map[string][]string /* author -> commits */
	lines   map[string]int      /* commit -> #lines */
	files   map[string]int      /* author -> #files */
}

func NewFameTable() *FameTable {
	return &FameTable{
		make(map[string][]string),
		make(map[string]int),
		make(map[string]int),
	}
}

func (t *FameTable) AssignCommit(author, commit string) {
	if _, ok := t.commits[author]; !ok {
		t.commits[author] = make([]string, 0, 1)
	}

	t.commits[author] = append(t.commits[author], commit)
}

func (t *FameTable) CommitLines(commit string, lines int) {
	t.lines[commit] = lines
}

func (t *FameTable) AddFile(author, file string) {
	if _, ok := t.files[author]; !ok {
		t.files[author] = 0
	}

	t.files[author]++
}

func (t *FameTable) Accumulate(commits map[string][]string, lines map[string]int) {
	for author, cs := range commits {
		t.commits[author] = append(t.commits[author], cs...)
	}

	for commit, numLines := range lines {
		t.lines[commit] += numLines
	}

	for author, cms := range commits {
		if len(cms) != 0 {
			t.files[author]++
		}
	}
}

func NotSupported() string {
	panic("Not supported")
}

type PersonStats struct {
	Name    string
	Lines   int
	Commits int
	Files   int
}

func LexicographicallyGreater(a1, a2, b1, b2, c1, c2 int, d1, d2 string) bool {
	if a1 == a2 {
		if b1 == b2 {
			if c1 == c2 {
				return strings.Compare(d1, d2) == -1
			}
			return c1 > c2
		}
		return b1 > b2
	}
	return a1 > a2
}

func (t *FameTable) Sorted(by string) []PersonStats {
	stats := make([]PersonStats, 0)

	for author := range t.commits {
		numFiles := t.files[author]
		numLines := t.CountLines(author)
		numCommits := t.CountCommits(author)

		stats = append(stats, PersonStats{
			Name:    author,
			Lines:   numLines,
			Commits: numCommits,
			Files:   numFiles,
		})
	}

	var comp func(i, j int) bool

	switch by {
	case "lines":
		comp = func(i, j int) bool {
			return LexicographicallyGreater(
				stats[i].Lines,
				stats[j].Lines,
				stats[i].Commits,
				stats[j].Commits,
				stats[i].Files,
				stats[j].Files,
				stats[i].Name,
				stats[j].Name,
			)
		}
	case "files":
		comp = func(i, j int) bool {
			return LexicographicallyGreater(
				stats[i].Files,
				stats[j].Files,
				stats[i].Lines,
				stats[j].Lines,
				stats[i].Commits,
				stats[j].Commits,
				stats[i].Name,
				stats[j].Name,
			)
		}
	case "commits":
		comp = func(i, j int) bool {
			return LexicographicallyGreater(
				stats[i].Commits,
				stats[j].Commits,
				stats[i].Lines,
				stats[j].Lines,
				stats[i].Files,
				stats[j].Files,
				stats[i].Name,
				stats[j].Name,
			)
		}
	default:
		panic("No comp!")
	}

	sort.Slice(stats, comp)
	return stats
}

func (t *FameTable) CastToJSONLines(sortedBy string) {
	for _, rec := range t.Sorted(sortedBy) {
		mapD := map[string]interface{}{"name": rec.Name, "lines": rec.Lines, "commits": rec.Commits, "files": rec.Files}
		mapB, err := json.Marshal(mapD)

		if err != nil {
			panic(err)
		}

		fmt.Println(string(mapB))
	}
}

func (t *FameTable) CastToCSV(sortedBy string) {
	writer := csv.NewWriter(os.Stdout)

	_ = writer.Write([]string{"Name", "Lines", "Commits", "Files"})
	for _, rec := range t.Sorted(sortedBy) {
		numFiles := strconv.Itoa(rec.Files)
		numLines := strconv.Itoa(rec.Lines)
		numCommits := strconv.Itoa(rec.Commits)

		_ = writer.Write([]string{rec.Name, numLines, numCommits, numFiles})
	}

	writer.Flush()
}

func (t *FameTable) CastToJSON(sortedBy string) {
	entries := make([]string, 0)

	for _, rec := range t.Sorted(sortedBy) {
		mapD := map[string]interface{}{"name": rec.Name, "lines": rec.Lines, "commits": rec.Commits, "files": rec.Files}
		mapB, err := json.Marshal(mapD)

		if err != nil {
			panic(err)
		}

		entries = append(entries, string(mapB))
	}

	fmt.Println("[" + strings.Join(entries, ",") + "]")
}

func (t *FameTable) CountCommits(author string) int {
	uniqueCommits := make(map[string]bool)
	commits := t.commits[author]

	for _, c := range commits {
		uniqueCommits[c] = true
	}

	return len(uniqueCommits)
}

func (t *FameTable) CountLines(author string) int {
	var (
		ok    bool
		count int = 0
	)

	commits, ok := t.commits[author]
	if !ok {
		return 0
	}

	uniqueCommits := make(map[string]bool)

	for _, c := range commits {
		uniqueCommits[c] = true
	}

	for c := range uniqueCommits {
		lines, ok := t.lines[c]
		if !ok {
			continue
		}

		count += lines
	}

	return count
}

func (t *FameTable) CastToTabular(sortedBy string) {
	writer := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)

	fmt.Fprint(writer, "Name\tLines\tCommits\tFiles")
	for _, rec := range t.Sorted(sortedBy) {
		fmt.Fprintf(writer, "\n%s\t%d\t%d\t%d", rec.Name, rec.Lines, rec.Commits, rec.Files)
	}

	fmt.Fprint(writer, "\n")
	writer.Flush()
}

type Config struct {
	repository string
	revision   string
	ordering   string
	committer  bool
	format     string
	extensions []string
	languages  []string
	exclude    []string
	restrict   []string
}

type LangRec struct {
	Name string   `json:"name"`
	Type string   `json:"type"`
	Exts []string `json:"extensions"`
}

type ExtensionFilter struct {
	mapping   map[string]bool
	acceptAll bool
}

func NewExtensionFilter(datapath string, langs []string) *ExtensionFilter {
	if len(langs) == 0 {
		return &ExtensionFilter{
			make(map[string]bool),
			true,
		}
	}

	mapping := make(map[string]bool)
	buffer := make([]LangRec, 0)

	file, err := os.Open(datapath)
	if err != nil {
		panic(err)
	}

	langsSet := make(map[string]bool)
	for _, lang := range langs {
		langsSet[strings.ToLower(lang)] = true
	}

	dec := json.NewDecoder(file)
	_ = dec.Decode(&buffer)

	for _, rec := range buffer {
		lang := strings.ToLower(rec.Name)
		if _, ok := langsSet[lang]; ok {
			for _, ext := range rec.Exts {
				mapping[ext] = true
			}
		}
	}

	return &ExtensionFilter{mapping, false}
}

func (flt *ExtensionFilter) Filter(input, exts []string) []string {
	out := make([]string, 0)

	// Transform to set
	extsSet := make(map[string]bool)
	for _, elem := range exts {
		extsSet[elem] = true
	}

	for _, filename := range input {
		ext := filepath.Ext(filename)
		// Check language
		if _, ok := flt.mapping[ext]; ok || flt.acceptAll {
			// Check 'exts'
			if _, ok := extsSet[ext]; ok || len(exts) == 0 {
				out = append(out, filename)
			}
		}
	}

	return out
}

func ExcludeRestrict(input, restrict, exclude []string) []string {
	out := make([]string, 0)

	for _, filename := range input {
		var (
			pass     bool
			ok       bool
			excluded bool
		)

		for _, pattern := range exclude {
			if ok, _ = filepath.Match(pattern, filename); ok {
				excluded = true
			}
		}

		if excluded {
			continue
		}

		for _, pattern := range restrict {
			if ok, _ = filepath.Match(pattern, filename); ok {
				pass = true
			}
		}

		if len(restrict) == 0 {
			pass = true
		}

		if pass {
			out = append(out, filename)
		}
	}

	return out
}

///////////////////////////////////////////////////////////

// Global
var config Config
var languageExtensionsPath string = "../../configs/language_extensions.json"

func Report(t *FameTable) {
	switch config.format {
	case "json":
		t.CastToJSON(config.ordering)
	case "json-lines":
		t.CastToJSONLines(config.ordering)
	case "csv":
		t.CastToCSV(config.ordering)
	case "tabular":
		t.CastToTabular(config.ordering)
	default:
		panic("Unknown format \"" + config.format + "\"")
	}
}

func SetupConfig(c Config) {
	config = c
}

func GetGlobMatches(patterns []string) map[string]bool {
	answer := make(map[string]bool)

	for _, pattern := range patterns {
		matches, _ := filepath.Glob(filepath.Join(config.repository, pattern))
		for _, path := range matches {
			answer[path] = true
		}
	}

	return answer
}

func Extension(filename string) string {
	panic("implement me")
}

func Language(filename string) string {
	panic("implement me")
}

func RepositoryFiles() []string {
	command := exec.Command("git", "ls-tree", "-r", "--name-only", config.revision)
	command.Dir = config.repository

	output, err := command.Output()
	if err != nil {
		panic(err)
	}

	// globExclusions := GetGlobMatches(config.exclude)
	// globRestrictions := GetGlobMatches(config.restrict)
	// extensions := config.extensions
	// languages := config.languages

	answer := make([]string, 0)

	for _, name := range strings.Split(string(output), "\n") {
		if name == "" {
			continue
		}

		answer = append(answer, name)
	}

	return answer
}

func GitLogParse(line string) (author, hash string) {
	words := strings.Split(line, " ")
	hash = words[0]
	author = strings.Join(words[1:], " ")
	return
}

func GitLog(file, rev, repo string) []string {
	command := exec.Command("git", "log", "--pretty=format:%H %an", rev, "--", file)
	command.Dir = repo

	output, err := command.Output()
	if err != nil {
		panic(err)
	}

	lines := strings.Split(string(output), "\n")
	return lines
}

// func GitBlame(file, rev, repo string, usecommitter bool) FileBlameRecord {
// 	if file == "" {
// 		return NewFileRecord(0, file)
// 	}

// 	command := exec.Command("git", "blame", "--porcelain", file, rev)
// 	command.Dir = repo

// 	output, err := command.Output()
// 	if err != nil {
// 		panic(err)
// 	}

// 	lines := strings.Split(string(output), "\n")
// 	numLines := len(lines)
// 	record := NewFileRecord(numLines, file)

// 	// Maybe in future...

// 	panic("implement me")
// }

func GitBlame(file string) []string {
	cmd := exec.Command("git", "blame", "--porcelain", file, config.revision)
	cmd.Dir = config.repository

	output, err := cmd.Output()
	if err != nil {
		panic(err)
	}

	if len(output) == 0 {
		return make([]string, 0)
	}
	return strings.Split(string(output), "\n")
}

func ParseGitBlame(output []string) (map[string][]string, map[string]int) {
	commits := make(map[string][]string)
	lines := make(map[string]int)

	var (
		seekAuthor  bool   = false
		nextHash    bool   = true
		commitLines int    = 0
		prevHash    string = ""
	)

	for _, line := range output {
		if line == "" {
			continue
		}

		if nextHash {
			nextHash = false
			if commitLines == 0 {
				words := strings.Fields(line)
				commitLines, _ = strconv.Atoi(words[len(words)-1])

				hash := words[0]
				lines[hash] += commitLines
				seekAuthor = true
				prevHash = hash
			}
			commitLines--
		} else if line[0] != '\t' && seekAuthor {
			words := strings.Fields(line)
			marker := words[0]
			var name string

			if !config.committer && marker == "author" {
				name = line[len("author "):]
				commits[name] = append(commits[name], prevHash)
				seekAuthor = false
			} else if config.committer && marker == "committer" {
				name = line[len("committer "):]
				commits[name] = append(commits[name], prevHash)
				seekAuthor = false
			}
		} else if line[0] == '\t' {
			nextHash = true
		}
	}

	return commits, lines
}

func CollectFileInfo(file string, t *FameTable) {
	output := GitBlame(file)

	var (
		commits map[string][]string
		lines   map[string]int
	)

	if len(output) == 0 {
		log := GitLog(file, config.revision, config.repository)
		author, hash := GitLogParse(log[0])

		commits = map[string][]string{
			author: {hash},
		}

		lines = map[string]int{
			hash: 0,
		}
	} else {
		commits, lines = ParseGitBlame(output)
	}

	t.Accumulate(commits, lines)
}

// TODO: Add a check for correct command
func IsCommand(line string) bool {
	return line[0] == '-' && line[1] == '-'
}

func IsFlag(line string) bool {
	return line == "--use-committer"
}

func ParseListToSlice(line string) []string {
	return strings.Split(line, ",")
}

func ParseList(line string) map[string]bool {
	extensions := strings.Split(line, ",")
	result := make(map[string]bool)

	for _, ext := range extensions {
		result[ext] = true
	}

	return result
}

func ParsePatterns(line string) map[string]bool {
	return make(map[string]bool)
}

func ParseCommandLine(args []string) {
	config = Config{}
	config.format = "tabular"
	config.revision = "HEAD"
	config.repository = ""
	config.ordering = "lines"

	extraStep := 0
	for i, numArgs := 0, len(args); i < numArgs; i += extraStep + 1 {
		extraStep = 0
		token := args[i]

		if !IsCommand(token) {
			panic("Unknown token at << " + token + " >>")
		}
		command := token

		arg := ""

		if !IsFlag(command) {
			// Argument exists
			if i+1 > numArgs {
				panic("No argument for << " + command + " >>")
			}
			extraStep = 1
			arg = args[i+1]
		}

		switch command {
		case "--repository":
			// TODO: Ensure correct path?
			config.repository = arg
		case "--revision":
			config.revision = arg
		case "--order-by":
			// Check later?
			if arg == "lines" || arg == "commits" || arg == "files" {
				config.ordering = arg
			} else {
				panic("--order-by " + arg + " – invalid value")
			}
		case "--use-committer":
			config.committer = true
		case "--format":
			// TODO: Ensure correct format?
			if arg == "json" || arg == "json-lines" || arg == "tabular" || arg == "csv" {
				config.format = arg
			} else {
				panic("--format " + arg + " – invalid value")
			}
		case "--extensions":
			config.extensions = ParseListToSlice(arg)
		case "--languages":
			config.languages = ParseListToSlice(arg)
		case "--exclude":
			config.exclude = ParseListToSlice(arg)
		case "--restrict-to":
			config.restrict = ParseListToSlice(arg)
		default:
			panic("No such command: " + command)
		}
	}
}

func Recover() {
	if r := recover(); r != nil {
		fmt.Println("Recovered: ", r)
		os.Exit(1)
		// Print usage?
	}
}

func realMain(args []string) {
	ParseCommandLine(args)
	defer Recover()

	flt := NewExtensionFilter(languageExtensionsPath, config.languages)
	repo := RepositoryFiles()
	filteredRepo := flt.Filter(repo, config.extensions)
	files := ExcludeRestrict(filteredRepo, config.restrict, config.exclude)

	table := NewFameTable()

	for _, file := range files {
		CollectFileInfo(file, table)
	}

	Report(table)
}

func main() {
	realMain(os.Args[1:])
}
