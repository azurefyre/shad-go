// +build !solution

package hogwarts

import (
	"fmt"
)

func Report(ans []string) {
	for _, elem := range ans {
		fmt.Println(elem)
	}
}

func TopSortImpl(node string, g map[string][]string,
	color map[string]int, answer *[]string) {
	if color[node] == 2 {
		return
	} else if color[node] == 1 {
		panic("Found cyclic dependency")
	}

	// fmt.Println("Visit", node)

	color[node] = 1
	for _, next := range g[node] {
		TopSortImpl(next, g, color, answer)
	}

	// Report(*answer)

	color[node] = 2
	*answer = append(*answer, node)
}

func MakeWhite(graph map[string][]string, verticesSet map[string]bool, color map[string]int) {
	for u, vs := range graph {
		verticesSet[u] = true
		for _, w := range vs {
			verticesSet[w] = true
		}
	}

	for u, present := range verticesSet {
		if present {
			color[u] = 0
		}
	}
}

func PickFirstUnvisited(verticesSet map[string]bool, color map[string]int) string {
	for v, present := range verticesSet {
		if present {
			if color[v] == 0 {
				return v
			}
		}
	}

	return ""
}

func TopSort(graph map[string][]string) []string {
	answer := []string{}
	color := make(map[string]int) // 0 - white, 1 - grey, 2 - black
	verticesSet := make(map[string]bool)
	MakeWhite(graph, verticesSet, color)

	// num_disciplines := len(verticesSet)

	for {
		node := PickFirstUnvisited(verticesSet, color)
		if node == "" {
			break // visited all vertices
		}

		TopSortImpl(node, graph, color, &answer)
	}

	return answer
}

func GetCourseList(prereqs map[string][]string) []string {
	return TopSort(prereqs)
}
