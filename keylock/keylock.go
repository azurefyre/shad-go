// +build !solution

package keylock

import (
	"sort"
	"sync"
)

type KeyLock struct {
	lockers sync.Map
	mu      *sync.Mutex
}

func New() *KeyLock {
	mu := &sync.Mutex{}

	return &KeyLock{
		lockers: sync.Map{},
		mu:      mu,
	}
}

func (l *KeyLock) UnlockPrefix(keys []string, lockedPrefix int) {
	for i := 0; i < lockedPrefix; i++ {
		u, ok := l.lockers.Load(keys[i])
		if !ok {
			panic("Couldn't load mutex in UnlockPrefix()")
		}
		// Release
		mu := u.(chan struct{})
		mu <- struct{}{}
	}
}

func (l *KeyLock) LockKeys(keys []string, cancel <-chan struct{}) (canceled bool, unlock func()) {
	sortedKeys := make([]string, len(keys))
	copy(sortedKeys, keys)
	sort.Strings(sortedKeys)

	for lockedPrefix, key := range sortedKeys {
		newLock := make(chan struct{}, 1)
		newLock <- struct{}{}

		v, _ := l.lockers.LoadOrStore(key, newLock)

		lock := v.(chan struct{})
		select {
		case <-lock:
			// Locked
		case <-cancel:
			// Cancelled
			l.UnlockPrefix(sortedKeys, lockedPrefix)
			canceled, unlock = true, func() {}
			return
		}
	}

	canceled = false
	unlock = func() {
		l.UnlockPrefix(sortedKeys, len(sortedKeys))
	}

	return
}
