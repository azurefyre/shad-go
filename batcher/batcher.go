// +build !solution

package batcher

import (
	"sync"

	"gitlab.com/slon/shad-go/batcher/slow"
)

type Batcher struct {
	numWaiters int
	mu         *sync.Mutex
	cond       *sync.Cond
	loader     *slow.Value
	lastValue  interface{}
	notify     chan interface{}
}

func NewBatcher(v *slow.Value) *Batcher {
	mu := &sync.Mutex{}

	return &Batcher{
		numWaiters: 0,
		loader:     v,
		mu:         mu,
		cond:       sync.NewCond(mu),
		notify:     make(chan interface{}),
		lastValue:  nil,
	}
}

func (b *Batcher) Load() interface{} {
	b.mu.Lock()
	b.numWaiters++

	if b.numWaiters == 1 {
		oldCounter := b.loader.Counter()

		var (
			counter int = -1
			value   interface{}
		)

		for oldCounter != counter {
			b.mu.Unlock()
			value, counter = b.loader.LoadWithCounter()
			b.mu.Lock()
		}

		b.numWaiters--
		b.mu.Unlock()

		numWaiters := b.numWaiters
		for i := 0; i < numWaiters; i++ {
			b.notify <- value
		}

		return value
	}

	b.mu.Unlock()

	value := <-b.notify

	b.mu.Lock()
	b.numWaiters--
	b.mu.Unlock()

	return value
}
