// +build !change

package slow

import (
	"sync"
	"sync/atomic"
	"time"
)

type Value struct {
	mu          sync.Mutex
	value       interface{}
	readRunning int32
	counter     int
}

func (s *Value) Load() interface{} {
	if atomic.SwapInt32(&s.readRunning, 1) == 1 {
		panic("another load is running")
	}
	defer atomic.StoreInt32(&s.readRunning, 0)

	s.mu.Lock()
	value := s.value
	s.mu.Unlock()

	time.Sleep(time.Millisecond)
	return value
}

func (s *Value) LoadWithCounter() (interface{}, int) {
	if atomic.SwapInt32(&s.readRunning, 1) == 1 {
		panic("another load is running")
	}
	defer atomic.StoreInt32(&s.readRunning, 0)

	s.mu.Lock()
	value := s.value
	counter := s.counter
	s.mu.Unlock()

	time.Sleep(time.Millisecond)
	return value, counter
}

func (s *Value) Counter() int {
	s.mu.Lock()
	defer s.mu.Unlock()

	counter := s.counter

	return counter
}

func (s *Value) Store(v interface{}) {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.counter++
	s.value = v
}
