// +build !solution

package reversemap

import (
	"reflect"
)

func ReverseMap(forward interface{}) interface{} {
	v := reflect.ValueOf(forward)
	switch v.Kind() {
	case reflect.Map:
		// Ok

		keyType := reflect.TypeOf(forward).Key()
		elemType := reflect.TypeOf(forward).Elem()
		mapType := reflect.MapOf(elemType, keyType)
		backward := reflect.MakeMap(mapType)

		forwardV := reflect.ValueOf(forward)

		for _, key := range forwardV.MapKeys() {
			value := forwardV.MapIndex(key)
			backward.SetMapIndex(value, key)
		}

		return backward.Interface()
	default:
		panic("'forward' argument must be a map")
	}
}
