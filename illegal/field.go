// +build !solution

package illegal

import (
	"reflect"
	"unsafe"
)

func SetPrivateField(obj interface{}, name string, value interface{}) {
	objv := reflect.ValueOf(obj).Elem()
	if objv.Kind() != reflect.Struct {
		panic("not struct")
	}

	f := objv.FieldByName(name)
	fp := unsafe.Pointer(f.UnsafeAddr())
	field := reflect.NewAt(reflect.TypeOf(value), fp).Elem()

	field.Set(reflect.ValueOf(value))
}
