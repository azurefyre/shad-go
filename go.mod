module gitlab.com/slon/shad-go

go 1.16

require (
	github.com/ClickHouse/clickhouse-go v1.4.0
	github.com/DATA-DOG/go-sqlmock v1.4.1
	github.com/alicebob/miniredis/v2 v2.14.3
	github.com/cenkalti/backoff v2.2.1+incompatible
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-redis/redis/v7 v7.4.0
	github.com/go-resty/resty/v2 v2.1.0
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/golang/mock v1.4.1
	github.com/gomodule/redigo v1.8.4
	github.com/google/go-cmp v0.4.0
	github.com/google/uuid v1.2.0
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/websocket v1.4.2
	github.com/jackc/pgx/v4 v4.6.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/jonboulle/clockwork v0.1.0
	github.com/lib/pq v1.10.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/spf13/cobra v1.1.3
	github.com/spf13/pflag v1.0.5
	github.com/stretchr/testify v1.7.0
	github.com/syndtr/goleveldb v1.0.0
	github.com/testcontainers/testcontainers-go v0.9.0
	go.etcd.io/bbolt v1.3.2
	go.uber.org/goleak v1.0.0
	go.uber.org/zap v1.14.0
	golang.org/x/net v0.0.0-20201021035429-f5854403a974
	golang.org/x/perf v0.0.0-20191209155426-36b577b0eb03
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	golang.org/x/sys v0.0.0-20210119212857-b64e53b001e4
	golang.org/x/tools v0.1.0
	gopkg.in/src-d/go-git.v4 v4.13.1
	gopkg.in/yaml.v2 v2.4.0
	gopl.io v0.0.0-20200323155855-65c318dde95e
	honnef.co/go/tools v0.1.3
)
