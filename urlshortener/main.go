// +build !solution

package main

import (
	"context"
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"
)

var (
	keysToUrls sync.Map
	numUrls    int = 0
)

////////////////////////////////////////////////////////////////

func AddKey(key string, url string) {
	keysToUrls.Store(key, url)
}

func Hash(url string) string {
	hasher := sha1.New()
	_, _ = hasher.Write([]byte(url))
	return hex.EncodeToString(hasher.Sum(nil))
}

func NewKey(url string) string {
	hash := Hash(url)
	key := hash[:6]
	return key
}

func Shorten(url string) (string, error) {
	key := NewKey(url)
	AddKey(key, url)
	return key, nil
}

func getURLForKey(key string) (string, error) {
	url, ok := keysToUrls.Load(key)
	if !ok {
		return "", errors.New("couldn't find url for key")
	}

	return url.(string), nil
}

func Go(key string) (url string, err error) {
	return "", nil
}

func getLongURL(r *http.Request) (string, error) {
	type ShortenRequest struct {
		URL string `json:"url"`
	}

	s := new(ShortenRequest)
	err := json.NewDecoder(r.Body).Decode(&s)

	return s.URL, err
}

func checkMethod(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodGet && r.Method != http.MethodPost {
			rw.WriteHeader(http.StatusMethodNotAllowed)
			return
		}
		next.ServeHTTP(rw, r)
	})
}

type ShortenResponse struct {
	URL string `json:"url"`
	Key string `json:"key"`
}

func NewShortenResponse(url string, key string) ShortenResponse {
	return ShortenResponse{URL: url, Key: key}
}

func GetMethod(path string) string {
	return strings.Split(path[1:], "/")[0]
}

func GetKey(path string) (key string, err error) {
	splitted := strings.Split(path[1:], "/")

	if len(splitted) < 2 {
		err = errors.New("< 2 fields in a shorten request")
		key = ""
		return
	}

	key = splitted[1]
	err = nil
	return
}

func RequestHandler(w http.ResponseWriter, r *http.Request) {
	u, err := url.Parse(r.RequestURI)
	if err != nil {
		panic(err)
	}

	fmt.Println("handling...")

	switch GetMethod(u.Path) {
	case "shorten":
		fmt.Println("shorten")

		longURL, err := getLongURL(r)
		fmt.Println(longURL)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		key, err := Shorten(longURL)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)

		// Respond
		err = json.NewEncoder(w).Encode(NewShortenResponse(longURL, key))
		if err != nil {
			fmt.Printf("something went wrong while encoding")
			panic(err)
		}
	case "go":
		fmt.Println("go")

		key, err := GetKey(u.Path)
		if err != nil {
			fmt.Println("error while getting the key")
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		to, err := getURLForKey(key)
		if err != nil {
			fmt.Println("couldn't find a key")
			w.WriteHeader(http.StatusNotFound)
			return
		}

		w.Header().Set("Location", to)
		w.WriteHeader(http.StatusFound)

		buttonHTML := "<a href=\"" + to + "\">Found</a>"
		fmt.Fprint(w, buttonHTML)
	default:
		// Bad request
		w.WriteHeader(http.StatusBadRequest)
	}
}

func RunURLShortener(port string) error {
	srv := &http.Server{
		Addr:    ":" + port,
		Handler: checkMethod(http.HandlerFunc(RequestHandler)),
	}

	serveChan := make(chan error, 1)

	go func() {
		serveChan <- srv.ListenAndServe()
	}()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGINT, syscall.SIGTERM)

	var err error

	select {
	case <-stop:
		fmt.Println("shutting down gracefully")
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		err = srv.Shutdown(ctx)
	case err = <-serveChan:
		// Pass
	}

	return err
}

func parseArgs(args []string) string {
	var (
		port string
	)

	if len(args) < 2 {
		panic("Too few arguments: missing '-port <value>'")
	}

	for i := 0; i < len(args); i++ {
		arg := args[i]

		if arg == "-port" {
			// Pick next if exists
			if i+1 >= len(args) {
				panic("Wrong signature")
			}

			port = args[i+1]
		}
	}

	return port
}

func main() {
	port := parseArgs(os.Args[1:])
	err := RunURLShortener(port)

	if err != nil {
		panic(err)
	}

	var t interface{}

	// client := &http.Client{Transport: &http.Transport{MaxConnsPerHost: 100}}
}
