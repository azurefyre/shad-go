// +build !solution

package jsonrpc

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"reflect"
)

type RPCResponse struct {
	Content string
	Err     string
}

// RPC method signature:
// Method(ctx context.Context, req *Request) (*Response, error)

func IsContextArg(argtype reflect.Type) bool {
	return argtype == reflect.TypeOf(context.Background())
}

func IsStructArg(argtype reflect.Type) bool {
	if argtype.Kind() != reflect.Ptr {
		return false
	}

	return argtype.Elem().Kind() == reflect.Struct
}

func IsRPCMethod(method reflect.Method) bool {
	rightNumFields := method.Type.NumField() == 2
	rightFirstArg := IsContextArg(method.Type.In(0))
	rightSecondArg := IsStructArg(method.Type.In(1))
	rightOutput := IsStructArg(method.Type.Out(0))
	rightOutFields := method.Type.NumOut() == 2

	return rightNumFields && rightFirstArg && rightSecondArg && rightOutput && rightOutFields
}

func MakeHandler(service interface{}) http.Handler {
	rpcMethods := make(map[string]reflect.Value)
	requestType := make(map[string]reflect.Type)
	// responseType := make(map[string]reflect.Type)

	v := reflect.ValueOf(service)
	for i := 0; i < v.NumMethod(); i++ {
		method := v.Type().Method(i)
		//if IsRPCMethod(method) {
		methodName := method.Name
		rpcMethods[methodName] = v.Method(i)
		requestType[methodName] = v.Type().Method(i).Type.In(2)
		// responseType[methodName] = v.Type().Method(i).Type.Out(1)
		//}
	}

	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		methodToCall := req.URL.Path[1:]
		if call, ok := rpcMethods[methodToCall]; ok {
			// Call

			reqt := requestType[methodToCall]
			requestFmted := reflect.New(reqt)

			decoder := json.NewDecoder(req.Body)

			// q, _ := ioutil.ReadAll(req.Body)
			// fmt.Printf("%q", q)

			e := decoder.Decode(requestFmted.Interface())
			if e != nil {
				fmt.Println(e.Error())
				fmt.Println(requestFmted.Elem().Interface())
				panic(e)
			}

			// fmt.Println(requestFmted.Elem().Interface())

			args := []reflect.Value{reflect.ValueOf(req.Context()), requestFmted.Elem()}

			output := call.Call(args)

			if len(output) != 2 {
				w.WriteHeader(http.StatusConflict)
				return
			}

			value, err := output[0].Interface(), output[1].Interface()
			errorText := ""
			if err != nil {
				errorText = err.(error).Error()
			}

			fmt.Println(value, err)

			valueEncoded, _ := json.Marshal(value)
			response := RPCResponse{Content: string(valueEncoded), Err: errorText}

			w.WriteHeader(http.StatusOK)

			// var buffer bytes.Buffer

			encoder := json.NewEncoder(w)
			e = encoder.Encode(response)
			if e != nil {
				panic("something went wrong")
			}

			// q, _ := ioutil.ReadAll(&buffer)
			// fmt.Printf("%q", q)

			return
		}

		w.WriteHeader(http.StatusBadRequest)
	})
}

func CreateURL(endpoint, method string) string {
	return endpoint + "/" + method
}

// Call(ctx, server.URL, "Ping", &req, &rsp))

func Call(ctx context.Context, endpoint string, method string, req, rsp interface{}) error {
	var requestBody bytes.Buffer

	encoder := json.NewEncoder(&requestBody)
	_ = encoder.Encode(req)

	fmt.Println(req)

	request, err := http.NewRequestWithContext(
		ctx,
		"POST",
		CreateURL(endpoint, method),
		&requestBody,
	)

	if err != nil {
		return err
	}

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return err
	}

	// q, _ := ioutil.ReadAll(response.Body)
	// fmt.Printf("%q", q)

	rpcresponse := new(RPCResponse)

	decoder := json.NewDecoder(response.Body)
	_ = decoder.Decode(rpcresponse)

	fmt.Println(rpcresponse.Content)
	fmt.Println(rpcresponse.Err)

	_ = json.Unmarshal([]byte(rpcresponse.Content), rsp)

	err = nil
	if rpcresponse.Err != "" {
		err = errors.New(rpcresponse.Err)
	}

	return err
}
