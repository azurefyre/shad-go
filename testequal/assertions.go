// +build !solution

package testequal

import (
	"fmt"
)

func FormatMsg(msgAndArgs ...interface{}) string {
	l := len(msgAndArgs)
	switch {
	case l <= 0:
		return ""
	case l == 1:
		return msgAndArgs[0].(string)
	default:
		format := msgAndArgs[0].(string)
		return fmt.Sprintf(format, msgAndArgs[1:]...)
	}
}

func IsPointer(v interface{}) bool {
	switch v.(type) {
	case *struct{}:
		return true
	}

	return false
}

func GetType(v interface{}) string {
	if v == nil {
		return "nil"
	}

	switch v.(type) {
	case string:
		return "string"
	case map[string]string:
		return "map"
	case []int:
		return "int slice"
	case []byte:
		return "byte slice"
	case int:
		return "int"
	case int8:
		return "int"
	case int16:
		return "int"
	case int32:
		return "int"
	case int64:
		return "int"
	case uint:
		return "int"
	case uint8:
		return "int"
	case uint16:
		return "int"
	case uint32:
		return "int"
	case uint64:
		return "int"
	default:
		return "dont know"
	}
}

func GetTypeForShit(v interface{}) string {
	if _, ok := v.([]interface{}); ok {
		if len(v.([]interface{})) == 0 {
			return "empty slice"
		}
		panic("WTF")
	}

	return "pass"
}

func MapOneSidedInclosure(l, r map[string]string) bool {
	for k, want := range l {
		got, ok := r[k]
		if !ok {
			return false
		}

		if want != got {
			return false
		}
	}

	return true
}

func EqualMaps(l, r map[string]string) bool {
	if l == nil || r == nil {
		return false
	}

	if !MapOneSidedInclosure(l, r) {
		return false
	}

	if !MapOneSidedInclosure(r, l) {
		return false
	}

	return true
}

func EqualIntSlice(l, r []int) bool {
	if l == nil || r == nil {
		return false
	}

	if len(l) != len(r) {
		return false
	}

	for i := 0; i < len(l); i++ {
		if l[i] != r[i] {
			return false
		}
	}

	return true
}

func EqualByteSlice(l, r []byte) bool {
	if l == nil || r == nil {
		return false
	}

	if len(l) != len(r) {
		return false
	}

	for i := 0; i < len(l); i++ {
		if l[i] != r[i] {
			return false
		}
	}

	return true
}

func IsUnsupported(u interface{}) bool {
	return u == struct{}{}
}

func IsCornerCase(u, v interface{}) bool {
	if v == nil || u == nil {
		return true
	}

	if IsUnsupported(u) || IsUnsupported(v) {
		return true
	}

	if IsPointer(u) && !IsPointer(v) {
		return true
	}

	if !IsPointer(u) && IsPointer(v) {
		return true
	}

	return false
}

func EqualCornerCases(u, v interface{}) bool {
	if v == nil || u == nil {
		return false
	}

	if IsUnsupported(u) || IsUnsupported(v) {
		return false
	}

	if IsPointer(u) && !IsPointer(v) {
		return false
	}

	if !IsPointer(u) && IsPointer(v) {
		return false
	}

	return true
}

func Equal(u, v interface{}) bool {
	if IsCornerCase(u, v) {
		return EqualCornerCases(u, v)
	}

	if GetType(u) != GetType(v) {
		return false
	}

	// Same type
	switch GetType(v) {
	case "int":
		return u == v
	case "string":
		return u == v
	case "map":
		l := u.(map[string]string)
		r := v.(map[string]string)
		return EqualMaps(l, r)
	case "int slice":
		l := u.([]int)
		r := v.([]int)
		return EqualIntSlice(l, r)
	case "byte slice":
		l := u.([]byte)
		r := v.([]byte)
		return EqualByteSlice(l, r)
	default:
		panic("Some unknown type in Equal(...)")
	}
}

// AssertEqual checks that expected and actual are equal.
//
// Marks caller function as having failed but continues execution.
//
// Returns true iff arguments are equal.
func AssertEqual(t T, expected, actual interface{}, msgAndArgs ...interface{}) bool {
	t.Helper()

	if !Equal(expected, actual) {
		msg := FormatMsg(msgAndArgs...)
		t.Errorf("not equal:\nexpected:\t%v\nactual:\t%v\nmessage:\t%q",
			expected,
			actual,
			msg)
		return false
	}
	return true
}

// AssertNotEqual checks that expected and actual are not equal.
//
// Marks caller function as having failed but continues execution.
//
// Returns true iff arguments are not equal.
func AssertNotEqual(t T, expected, actual interface{}, msgAndArgs ...interface{}) bool {
	t.Helper()

	if Equal(expected, actual) {
		msg := FormatMsg(msgAndArgs...)
		t.Errorf("equal:\nexpected:\t%v\nactual:\t%v\nmessage:\t%q",
			expected,
			actual,
			msg)
		return false
	}
	return true
}

// RequireEqual does the same as AssertEqual but fails caller test immediately.
func RequireEqual(t T, expected, actual interface{}, msgAndArgs ...interface{}) {
	t.Helper()

	if !Equal(expected, actual) {
		msg := FormatMsg(msgAndArgs...)
		t.Errorf("not equal:\nexpected:\t%v\nactual:\t%v\nmessage:\t%q",
			expected,
			actual,
			msg)
		t.FailNow()
	}
}

// RequireNotEqual does the same as AssertNotEqual but fails caller test immediately.
func RequireNotEqual(t T, expected, actual interface{}, msgAndArgs ...interface{}) {
	t.Helper()

	if Equal(expected, actual) {
		msg := FormatMsg(msgAndArgs...)
		t.Errorf("equal:\nexpected:\t%v\nactual:\t%v\nmessage:\t%q",
			expected,
			actual,
			msg)
		t.FailNow()
	}
}
