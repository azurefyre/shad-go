// +build !solution

package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

func Check(e error) {
	if e != nil {
		panic(e)
	}
}

func Report(s string) {
	fmt.Println(s)
}

func Fetch(url string) string {
	reply, error := http.Get(url)
	Check(error)

	defer reply.Body.Close()

	content, err := ioutil.ReadAll(reply.Body)
	Check(err)

	return string(content)
}

func main() {
	if len(os.Args) <= 1 {
		os.Exit(0)
	}

	urls := os.Args[1:]

	for _, url := range urls {
		Report(Fetch(url))
	}
}
