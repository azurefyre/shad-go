// +build !solution

package jsonlist

import (
	"encoding/json"
	"fmt"
	"io"
	"reflect"
)

func Marshal(w io.Writer, slice interface{}) error {
	v := reflect.ValueOf(slice)
	switch v.Kind() {
	case reflect.Slice:
		// elemType := v.Type().Elem()
		encoder := json.NewEncoder(w)

		for i := 0; i < v.Len(); i++ {
			elem := v.Index(i)
			err := encoder.Encode(elem.Interface())
			if err != nil {
				return err
			}

			if i != v.Len()-1 {
				fmt.Fprint(w, " ")
			}
		}
	default:
		return &json.UnsupportedTypeError{Type: v.Type()}
	}

	return nil
}

func Unmarshal(r io.Reader, slice interface{}) error {
	in := reflect.ValueOf(slice)
	if in.Kind() != reflect.Ptr {
		return &json.UnsupportedTypeError{Type: in.Type()}
	}

	sliceValue := in.Elem()

	if sliceValue.Kind() != reflect.Slice {
		panic("not slice")
	}

	sliceElemType := sliceValue.Type().Elem()
	decoder := json.NewDecoder(r)
	var (
		elem reflect.Value = reflect.New(sliceElemType)
		err  error
	)

	err = decoder.Decode(elem.Interface())
	for ; err == nil; err = decoder.Decode(elem.Interface()) {
		// internal := elem.Elem().Interface()
		// fmt.Printf("--- %v", internal)

		defer func() {
			if e := recover(); e != nil {
				fmt.Println(e)
			}
		}()

		// Append to slice
		sliceValue.Set(reflect.Append(sliceValue, elem.Elem()))

		elem = reflect.New(sliceElemType)
	}

	if err == io.EOF {
		// ok
		return nil
	} else if err != nil {
		return err
	}

	return nil
}
