// +build !solution

package filecache

import (
	"errors"
	"io"
	"os"
	"path/filepath"

	"gitlab.com/slon/shad-go/distbuild/pkg/artifact"
	"gitlab.com/slon/shad-go/distbuild/pkg/build"
)

var (
	ErrNotFound    = errors.New("file not found")
	ErrExists      = errors.New("file exists")
	ErrWriteLocked = errors.New("file is locked for write")
	ErrReadLocked  = errors.New("file is locked for read")
)

func mapErrors(err error) error {
	switch err {
	case artifact.ErrExists:
		return ErrExists
	case artifact.ErrNotFound:
		return ErrNotFound
	case artifact.ErrReadLocked:
		return ErrReadLocked
	case artifact.ErrWriteLocked:
		return ErrWriteLocked
	default:
		panic("No map for this error")
	}
}

type Cache struct {
	c       *artifact.Cache
	rootDir string
}

func New(rootDir string) (*Cache, error) {
	c, err := artifact.NewCache(rootDir)
	if err != nil {
		return nil, err
	}
	return &Cache{c, rootDir}, nil
}

func (c *Cache) Range(fileFn func(file build.ID) error) error {
	return c.c.Range(fileFn)
}

func (c *Cache) Remove(file build.ID) error {
	return c.c.Remove(file)
}

type filecacheWriteCloser struct {
	commit func() error
	writer io.WriteCloser
}

func NewFilecacheWriteCloser(w io.WriteCloser, commit func() error) *filecacheWriteCloser {
	return &filecacheWriteCloser{commit, w}
}

func (wc *filecacheWriteCloser) Write(p []byte) (int, error) {
	return wc.writer.Write(p)
}

func (wc *filecacheWriteCloser) Close() error {
	var err error

	err = wc.commit()
	if err != nil {
		return err
	}

	err = wc.writer.Close()
	if err != nil {
		return err
	}

	return nil
}

func (c *Cache) Exists(file build.ID) bool {
	_, _, abort, err := c.c.Create(file)
	if err == nil {
		abort()
		return false
	} else if mapErrors(err) == ErrExists {
		return true
	}

	panic("Exists() – something's wrong: " + err.Error())
}

func (c *Cache) Write(file build.ID) (w io.WriteCloser, abort func() error, err error) {
	path, commit_impl, abort_impl, err := c.c.Create(file)
	if err != nil {
		err = mapErrors(err)
		return
	}

	// Create or truncate
	fd, err := os.Create(filepath.Join(path, "file"))
	if err != nil {
		return
	}

	w = NewFilecacheWriteCloser(fd, commit_impl)
	abort = abort_impl
	err = nil
	return
}

func (c *Cache) Get(file build.ID) (path string, unlock func(), err error) {
	path, unlock, err = c.c.Get(file)
	if err != nil {
		err = mapErrors(err)
	}

	path = filepath.Join(path, "file")

	return
}
