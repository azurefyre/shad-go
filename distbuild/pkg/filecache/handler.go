// +build !solution

package filecache

import (
	"io"
	"net/http"
	"os"

	"golang.org/x/sync/singleflight"

	"gitlab.com/slon/shad-go/distbuild/pkg/build"
	"go.uber.org/zap"
)

type Handler struct {
	c     *Cache
	l     *zap.Logger
	group *singleflight.Group
}

func (h *Handler) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	file := req.URL.Query().Get("id")
	if file == "" {
		rw.WriteHeader(http.StatusBadRequest)
		return
	}

	fileID := build.NewID()
	fileID.UnmarshalText([]byte(file))

	switch req.Method {
	case "GET":
		path, unlock, err := h.c.Get(fileID)
		if err != nil {
			rw.WriteHeader(http.StatusNotFound)
			return
		}

		fd, err := os.Open(path)
		if err != nil {
			rw.WriteHeader(http.StatusBadRequest)
			return
		}

		rw.WriteHeader(http.StatusOK)
		io.Copy(rw, fd)

		fd.Close()
		unlock()
	case "PUT":
		res, _, _ := h.group.Do(fileID.String(), func() (interface{}, error) {
			if h.c.Exists(fileID) {
				return http.StatusOK, nil
			}

			w, abort, err := h.c.Write(fileID)
			if err != nil {
				return http.StatusBadRequest, nil
			}

			_, err = io.Copy(w, req.Body)
			if err != nil {
				abort()
				return http.StatusExpectationFailed, nil
			}

			w.Close()
			return http.StatusOK, nil
		})

		code := res.(int)
		rw.WriteHeader(code)
		return
	default:
		rw.WriteHeader(http.StatusForbidden)
		return
	}
}

func NewHandler(l *zap.Logger, cache *Cache) *Handler {
	return &Handler{cache, l, &singleflight.Group{}}
}

func (h *Handler) Register(mux *http.ServeMux) {
	mux.Handle("/file", http.Handler(h))
}
