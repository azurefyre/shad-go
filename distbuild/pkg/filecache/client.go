// +build !solution

package filecache

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"

	"go.uber.org/zap"

	"gitlab.com/slon/shad-go/distbuild/pkg/build"
)

type Client struct {
	endpoint string
	l        *zap.Logger
}

func NewClient(l *zap.Logger, endpoint string) *Client {
	return &Client{endpoint, l}
}

func (c *Client) Upload(ctx context.Context, id build.ID, localPath string) error {
	uri := c.endpoint + fmt.Sprintf("/file?id=%s", id.String())
	file, err := os.Open(localPath)
	if err != nil {
		return err
	}

	req, err := http.NewRequestWithContext(ctx, "PUT", uri, file)
	if err != nil {
		return err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		msg := make([]byte, resp.ContentLength)
		resp.Body.Read(msg)
		return errors.New("upload failed: " + string(msg))
	}

	return nil
}

func (c *Client) Download(ctx context.Context, localCache *Cache, id build.ID) error {
	w, abort, err := localCache.Write(id)
	if err != nil {
		return err
	}

	uri := c.endpoint + fmt.Sprintf("/file?id=%s", id.String())
	req, err := http.NewRequestWithContext(ctx, "GET", uri, nil)
	if err != nil {
		abort()
		return err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		abort()
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return errors.New("download failed")
	}

	io.Copy(w, resp.Body)
	w.Close()

	return nil
}
