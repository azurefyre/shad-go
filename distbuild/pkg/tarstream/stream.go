// +build !solution

package tarstream

import (
	"archive/tar"
	"fmt"
	"io"
	"io/fs"
	"log"
	"os"
	"path/filepath"
)

func Send(dir string, w io.Writer) error {
	var stat os.FileInfo
	var err error
	if stat, err = os.Stat(dir); err != nil {
		panic(err)
	}

	if !stat.IsDir() {
		panic("send(): not dir")
	}

	tw := tar.NewWriter(w)

	walkfunc := func(path string, info fs.FileInfo, in_err error) error {
		var err error

		// Root dir
		if path == dir {
			return nil
		}

		relativePath := path[len(dir):]

		hdr := &tar.Header{
			Name: relativePath,
			Mode: int64(info.Mode().Perm()),
			Size: info.Size(),
		}

		if info.IsDir() {
			hdr.Typeflag = tar.TypeDir
		}

		if err = tw.WriteHeader(hdr); err != nil {
			log.Fatal(err)
			return err
		}

		if info.IsDir() {
			return nil
		}

		var file *os.File
		if file, err = os.Open(path); err != nil {
			log.Fatal(err)
			return err
		}

		if _, err := io.Copy(tw, file); err != nil {
			log.Fatal(err)
			return err
		}

		file.Close()
		return nil
	}

	if err := filepath.Walk(dir, filepath.WalkFunc(walkfunc)); err != nil {
		log.Fatal(err)
		return err
	}

	if err := tw.Close(); err != nil {
		log.Fatal(err)
		return err
	}

	return nil
}

func Receive(dir string, r io.Reader) error {
	tr := tar.NewReader(r)

	for {
		hdr, err := tr.Next()
		if err == io.EOF {
			break // End of archive
		}

		path := dir + hdr.Name
		// fmt.Fprintf(os.Stderr, "%s\n", path)
		// fmt.Fprintf(os.Stderr, "%s", hdr.Name)

		if err != nil {
			log.Fatal(err)
			return err
		}

		if hdr.FileInfo().IsDir() {
			err = os.Mkdir(path, os.FileMode(hdr.Mode))
			if err != nil {
				fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			}
			continue
		}

		var file *os.File
		if file, err = os.Create(path); err != nil {
			log.Fatal(err)
			return err
		}

		if err = os.Chmod(path, hdr.FileInfo().Mode()); err != nil {
			log.Fatal(err)
			return err
		}

		if _, err = io.Copy(file, tr); err != nil {
			log.Fatal(err)
			return err
		}
	}

	return nil
}
