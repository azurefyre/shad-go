// +build !solution

package api

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"

	"go.uber.org/zap"
)

// Coordinator side

type HeartbeatHandler struct {
	l *zap.SugaredLogger
	s HeartbeatService
}

func (hh *HeartbeatHandler) ServeHTTP(rw http.ResponseWriter, httpreq *http.Request) {
	l := hh.l

	if httpreq.Method != "POST" {
		l.Warn("failing: not 'POST'")
		rw.WriteHeader(http.StatusForbidden)
		return
	}

	if httpreq.URL.Path != "/heartbeat" {
		l.Warn("failing: not '/heartbeat'")
		rw.WriteHeader(http.StatusForbidden)
		return
	}

	var req HeartbeatRequest

	dec := json.NewDecoder(httpreq.Body)
	err := dec.Decode(&req)
	if err != nil {
		l.Warn("failing: coulnd't decode")
		rw.WriteHeader(http.StatusExpectationFailed)
		return
	}

	resp, err := hh.s.Heartbeat(httpreq.Context(), &req)
	if err != nil {
		l.Warn("got error from 'hh.s.Heartbeat(...)'")
		rw.WriteHeader(http.StatusExpectationFailed)
		rw.Write([]byte("build error: " + err.Error()))
		return
	}

	var buf bytes.Buffer
	rw.WriteHeader(http.StatusOK)
	enc := json.NewEncoder(&buf)
	enc.Encode(resp)
	l.Info("handling Heartbeat request")
	l.Debug(buf.String())
	io.Copy(rw, &buf)
}

func NewHeartbeatHandler(l *zap.Logger, s HeartbeatService) *HeartbeatHandler {
	return &HeartbeatHandler{
		l.WithOptions(zap.AddCaller(), zap.AddCallerSkip(1)).Sugar().Named("Master"),
		s,
	}
}

func (h *HeartbeatHandler) Register(mux *http.ServeMux) {
	mux.Handle("/heartbeat", h)
}
