// +build !solution

package api

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/slon/shad-go/distbuild/pkg/build"
)

type BuildClient struct {
	l        *zap.SugaredLogger
	endpoint string
}

func NewBuildClient(l *zap.Logger, endpoint string) *BuildClient {
	return &BuildClient{l.Sugar(), endpoint}
}

type streamStatusReader struct {
	resp   *http.Response
	dec    *json.Decoder
	l      *zap.SugaredLogger
	closed bool
}

func NewStreamStatusReader(resp *http.Response, l *zap.SugaredLogger, dec *json.Decoder) *streamStatusReader {
	return &streamStatusReader{resp, dec, l, false}
}

func (s *streamStatusReader) Close() error {
	if s.closed {
		return errors.New("closed")
	}

	s.resp.Body.Close()
	return nil
}

func (s *streamStatusReader) Next() (*StatusUpdate, error) {
	if s.closed {
		return nil, errors.New("closed")
	}

	su := new(StatusUpdate)
	err := s.dec.Decode(su)
	if err != nil {
		return nil, err
	}

	return su, nil
}

func (c *BuildClient) StartBuild(ctx context.Context, request *BuildRequest) (*BuildStarted, StatusReader, error) {
	uri := c.endpoint + "/build"

	var (
		buf  bytes.Buffer
		err  error
		resp *BuildStarted = new(BuildStarted)
	)

	enc := json.NewEncoder(&buf)
	enc.Encode(request)

	httpreq, err := http.NewRequestWithContext(ctx, "POST", uri, &buf)
	if err != nil {
		return nil, nil, err
	}

	c.l.Info("Send build request to " + c.endpoint)

	httpresp, err := http.DefaultClient.Do(httpreq)
	if err != nil {
		return nil, nil, err
	}

	if httpresp.StatusCode != http.StatusOK {
		// Extract error message from 'BuildFailed'
		// var bf BuildFailed
		// dec := json.NewDecoder(httpresp.Body)
		// dec.Decode(&bf)
		// return nil, nil, errors.New(bf.Error)
		msg, _ := ioutil.ReadAll(httpresp.Body)
		return nil, nil, errors.New(string(msg))
	}

	c.l.Info("Got response from " + c.endpoint)

	dec := json.NewDecoder(httpresp.Body)
	dec.Decode(resp)

	return resp, NewStreamStatusReader(httpresp, c.l, dec), nil
}

func (c *BuildClient) SignalBuild(ctx context.Context, buildID build.ID, signal *SignalRequest) (*SignalResponse, error) {
	uri := c.endpoint + fmt.Sprintf("/signal?build_id=%s", buildID.String())

	var (
		buf  bytes.Buffer
		err  error
		resp *SignalResponse
	)
	resp = new(SignalResponse)

	enc := json.NewEncoder(&buf)
	enc.Encode(signal)

	httpreq, err := http.NewRequestWithContext(ctx, "POST", uri, &buf)
	if err != nil {
		return nil, err
	}

	c.l.Info("Send signal build request to " + c.endpoint)

	httpresp, err := http.DefaultClient.Do(httpreq)
	if err != nil {
		return nil, err
	}

	// Слабое место
	c.l.Info("Got signal response from " + c.endpoint)

	if httpresp.StatusCode != http.StatusOK {
		msg, _ := ioutil.ReadAll(httpresp.Body)
		return nil, errors.New(string(msg))
	}

	dec := json.NewDecoder(httpresp.Body)
	err = dec.Decode(resp)
	if err != nil {
		c.l.Error("Couldn't decode json at SignalBuild")
		return nil, errors.New("couldn't decode json")
	}

	return resp, nil
}
