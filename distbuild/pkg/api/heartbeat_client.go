// +build !solution

package api

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
)

type HeartbeatClient struct {
	l        *zap.SugaredLogger
	endpoint string
}

func NewHeartbeatClient(l *zap.Logger, endpoint string) *HeartbeatClient {
	return &HeartbeatClient{l.Sugar().Named("Worker"), endpoint}
}

func (c *HeartbeatClient) Heartbeat(ctx context.Context, req *HeartbeatRequest) (*HeartbeatResponse, error) {
	uri := c.endpoint + "/heartbeat"

	var (
		buf  bytes.Buffer
		err  error
		resp *HeartbeatResponse
	)
	resp = new(HeartbeatResponse)

	enc := json.NewEncoder(&buf)
	enc.Encode(req)

	httpreq, err := http.NewRequestWithContext(ctx, "POST", uri, &buf)
	if err != nil {
		return nil, err
	}

	c.l.Info("Send heartbeat to " + c.endpoint)

	httpresp, err := http.DefaultClient.Do(httpreq)
	if err != nil {
		return nil, err
	}

	if httpresp.StatusCode != http.StatusOK {
		msg, _ := ioutil.ReadAll(httpresp.Body)
		err := errors.New(string(msg))
		return nil, err
	}

	c.l.Info("Got response from " + c.endpoint)

	dec := json.NewDecoder(httpresp.Body)
	dec.Decode(resp)

	return resp, nil
}
