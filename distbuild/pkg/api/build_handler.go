// +build !solution

package api

import (
	"encoding/json"
	"net/http"

	"gitlab.com/slon/shad-go/distbuild/pkg/build"
	"go.uber.org/zap"
)

func NewBuildService(l *zap.Logger, s Service) *BuildHandler {
	return &BuildHandler{l, s}
}

type BuildHandler struct {
	l *zap.Logger
	s Service
}

type streamStatusWriter struct {
	w     http.ResponseWriter
	enc   *json.Encoder
	wrote *bool
}

func NewStreamStatusWriter(w http.ResponseWriter, wrote *bool) *streamStatusWriter {
	return &streamStatusWriter{w, json.NewEncoder(w), wrote}
}

func (s *streamStatusWriter) Flush() {
	*s.wrote = true
	if f, ok := s.w.(http.Flusher); ok {
		f.Flush()
	}
}

func (s *streamStatusWriter) Started(rsp *BuildStarted) error {
	err := s.enc.Encode(rsp)
	if err == nil {
		s.Flush()
	}
	return err
}

func (s *streamStatusWriter) Updated(update *StatusUpdate) error {
	err := s.enc.Encode(update)
	if err == nil {
		s.Flush()
	}
	return err
}

func (bh *BuildHandler) ServeHTTP(rw http.ResponseWriter, httpreq *http.Request) {
	if httpreq.Method != "POST" {
		rw.WriteHeader(http.StatusForbidden)
		return
	}

	switch httpreq.URL.Path {
	case "/build":
		bh.l.Info("handling 'build' request")

		var br BuildRequest
		dec := json.NewDecoder(httpreq.Body)
		dec.Decode(&br)

		var wrote bool = false
		w := NewStreamStatusWriter(rw, &wrote)
		err := bh.s.StartBuild(httpreq.Context(), &br, w)

		if err != nil {
			bh.l.Warn(err.Error())
		}

		if err != nil && !wrote {
			rw.WriteHeader(http.StatusInternalServerError)
			rw.Write([]byte(err.Error()))
			return
		} else if err != nil {
			enc := json.NewEncoder(rw)
			enc.Encode(&StatusUpdate{
				BuildFailed: &BuildFailed{Error: err.Error()},
			})
			return
		}

		rw.WriteHeader(http.StatusOK)
	case "/signal":
		bh.l.Info("handling 'signal' request")

		buildIDstr := httpreq.URL.Query().Get("build_id")
		if buildIDstr == "" {
			rw.WriteHeader(http.StatusBadRequest)
			return
		}

		buildID := build.NewID()
		buildID.UnmarshalText([]byte(buildIDstr))

		var sr SignalRequest
		dec := json.NewDecoder(httpreq.Body)
		dec.Decode(&sr)

		resp, err := bh.s.SignalBuild(httpreq.Context(), buildID, &sr)
		if err != nil {
			rw.WriteHeader(http.StatusBadRequest)
			rw.Write([]byte(err.Error()))
			return
		}

		enc := json.NewEncoder(rw)
		err = enc.Encode(resp)

		if err != nil {
			rw.WriteHeader(http.StatusConflict)
			rw.Write([]byte(err.Error()))
			return
		}

		rw.WriteHeader(http.StatusOK)
	}
}

func (h *BuildHandler) Register(mux *http.ServeMux) {
	mux.Handle("/build", h)
	mux.Handle("/signal", h)
}
