// +build !solution

package artifact

import (
	"context"
	"fmt"
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/slon/shad-go/distbuild/pkg/build"
	"gitlab.com/slon/shad-go/distbuild/pkg/tarstream"
)

// Download artifact from remote cache into local cache.
func Download(ctx context.Context, endpoint string, c *Cache, artifactID build.ID) error {
	path, commit, abort, err := c.Create(artifactID)
	if err != nil {
		return err
	}

	uri := fmt.Sprintf("/artifact?id=%s", artifactID.String())
	queryUrl := endpoint + uri

	req, err := http.NewRequestWithContext(ctx, "GET", queryUrl, nil)
	if err != nil {
		abort()
		return err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		abort()
		return err
	}

	if resp.StatusCode != http.StatusOK {
		abort()
		return errors.Errorf("Couldn't fetch")
	}

	err = tarstream.Receive(path, resp.Body)
	if err != nil {
		abort()
		return err
	}

	commit()
	return nil
}
