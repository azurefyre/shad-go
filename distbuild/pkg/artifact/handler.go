// +build !solution

package artifact

import (
	"bytes"
	"io"
	"net/http"

	"gitlab.com/slon/shad-go/distbuild/pkg/build"
	"gitlab.com/slon/shad-go/distbuild/pkg/tarstream"
	"go.uber.org/zap"
)

type Handler struct {
	c *Cache
	l *zap.Logger
}

func (h *Handler) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	query := req.URL.Query()
	artifactID := query.Get("id")
	if artifactID == "" {
		panic("Request has no field `id`")
	}

	artifact := build.NewID()
	artifact.UnmarshalText([]byte(artifactID))

	path, unlock, err := h.c.Get(artifact)
	if err != nil {
		h.l.Debug("artifact.Get in ServeHTTP failed: " + err.Error())

		// Write elaborate error message to body
		rw.WriteHeader(http.StatusBadRequest)
		buf := bytes.NewBufferString(err.Error())
		io.Copy(rw, buf)
		return
	}

	rw.WriteHeader(http.StatusOK)
	if err := tarstream.Send(path, rw); err != nil {
		h.l.Debug("tarstream in ServeHTTP failed: " + err.Error())
		return
	}

	unlock()
}

func NewHandler(l *zap.Logger, c *Cache) *Handler {
	return &Handler{c, l}
}

func (h *Handler) Register(mux *http.ServeMux) {
	mux.Handle("/artifact", http.Handler(h))
}
