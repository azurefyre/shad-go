// +build !solution

package artifact

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"sync"

	"gitlab.com/slon/shad-go/distbuild/pkg/build"
)

var (
	ErrNotFound    = errors.New("artifact not found")
	ErrExists      = errors.New("artifact exists")
	ErrWriteLocked = errors.New("artifact is locked for write")
	ErrReadLocked  = errors.New("artifact is locked for read")
)

func stubFinalizer() error {
	return fmt.Errorf("No")
}

type artifactRecord struct {
	path    string
	locked  bool
	readers int
}

func NewArtifactRecord(path string) *artifactRecord {
	return &artifactRecord{path, false, 0}
}

func (ar *artifactRecord) Delete() error {
	err := os.RemoveAll(ar.path)
	// os.Remove(ar.path)

	return err
}

type Cache struct {
	root      string
	artifacts map[build.ID]*artifactRecord /* build.ID -> *artifactRecord */
	mu        *sync.Mutex
	dirPerm   os.FileMode
}

func NewCache(root string) (*Cache, error) {
	// err := os.Mkdir(root, os.FileMode(448))
	// if err != nil {
	// 	panic(err)
	// }

	rootInfo, err := os.Stat(root)
	if err != nil {
		panic(err)
	}

	defaultDirPerm := rootInfo.Mode().Perm()

	c := &Cache{
		root,
		make(map[build.ID]*artifactRecord),
		&sync.Mutex{},
		defaultDirPerm,
	}

	return c, nil
}

func (c *Cache) newDir(artifact build.ID) (string, error) {
	path := filepath.Join(c.root, artifact.String(), "")
	if err := os.Mkdir(path, c.dirPerm); err != nil {
		return "", err
	}
	return path, nil
}

func (c *Cache) Range(artifactFn func(artifact build.ID) error) error {
	for k, _ := range c.artifacts {
		err := artifactFn(k)
		if err != nil {
			return err
		}
	}
	return nil
}

func (c *Cache) removeImpl(artifact build.ID) error {
	artifactRec := c.artifacts[artifact]

	// Remove dir
	err := artifactRec.Delete()
	if err != nil {
		return err
	}

	// Remove record
	delete(c.artifacts, artifact)
	return nil
}

func (c *Cache) Remove(artifact build.ID) error {
	c.mu.Lock()
	defer c.mu.Unlock()

	var (
		ok  bool
		rec *artifactRecord
	)

	if rec, ok = c.artifacts[artifact]; !ok {
		return ErrNotFound
	}

	if rec.locked {
		return ErrWriteLocked
	}

	if rec.readers > 0 {
		return ErrReadLocked
	}

	c.removeImpl(artifact)

	return nil
}

func (c *Cache) Create(artifact build.ID) (path string, commit, abort func() error, err error) {
	c.mu.Lock()
	defer c.mu.Unlock()

	path = ""
	commit = stubFinalizer
	abort = stubFinalizer

	var (
		rec *artifactRecord
		ok  bool
	)

	if rec, ok = c.artifacts[artifact]; ok {
		if rec.locked {
			err = ErrWriteLocked
			return
		}

		if rec.readers > 0 {
			err = ErrReadLocked
			return
		}

		err = ErrExists
		return
	} else {
		path, err = c.newDir(artifact)
		if err != nil {
			commit = stubFinalizer
			abort = stubFinalizer
			return
		}

		c.artifacts[artifact] = NewArtifactRecord(path)
		rec = c.artifacts[artifact]
	}

	rec.locked = true
	finished := false

	commit = func() error {
		if finished {
			return errors.New("already finished")
		}

		c.mu.Lock()
		rec.locked = false
		c.mu.Unlock()

		return nil
	}

	abort = func() error {
		if finished {
			return errors.New("already finished")
		}

		c.mu.Lock()
		rec.locked = false
		c.removeImpl(artifact)
		c.mu.Unlock()
		return nil
	}

	return
}

func (c *Cache) Get(artifact build.ID) (path string, unlock func(), err error) {
	path = ""
	unlock = func() {}

	var (
		rec *artifactRecord
		ok  bool
	)

	c.mu.Lock()
	defer c.mu.Unlock()

	if rec, ok = c.artifacts[artifact]; !ok {
		err = ErrNotFound
		return
	}

	if rec.locked {
		err = ErrWriteLocked
		return
	}

	rec.readers++
	path = rec.path
	unlock = func() {
		c.mu.Lock()
		rec.readers--
		c.mu.Unlock()
	}
	err = nil

	return
}
