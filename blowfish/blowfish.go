// +build !solution

package blowfish

// #cgo LDFLAGS: -lcrypto
// #include <openssl/blowfish.h>
import "C"
import "unsafe"

type Blowfish struct {
	key C.BF_KEY
}

func (b *Blowfish) BlockSize() int {
	return 8
}

func (b *Blowfish) Encrypt(out, in []byte) {
	C.BF_ecb_encrypt(
		(*C.uchar)(unsafe.Pointer(&in[0])),
		(*C.uchar)(unsafe.Pointer(&out[0])),
		(*C.BF_KEY)(unsafe.Pointer(&b.key)),
		C.BF_ENCRYPT,
	)
}

func (b *Blowfish) Decrypt(out, in []byte) {
	C.BF_ecb_encrypt(
		(*C.uchar)(unsafe.Pointer(&in[0])),
		(*C.uchar)(unsafe.Pointer(&out[0])),
		(*C.BF_KEY)(unsafe.Pointer(&b.key)),
		C.BF_DECRYPT,
	)
}

func New(key []byte) *Blowfish {
	var bf Blowfish
	C.BF_set_key((*C.BF_KEY)(unsafe.Pointer(&bf.key)), (C.int)(len(key)), (*C.uchar)(unsafe.Pointer(&key[0])))
	return &bf
}
