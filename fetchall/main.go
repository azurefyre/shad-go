// +build !solution

package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

var start time.Time

func Check(e error) {
	if e != nil {
		fmt.Println(e)
		os.Exit(1)
	}
}

func StartTimer() {
	start = time.Now()
}

func Elapsed() int {
	now := time.Now()
	return int(now.Sub(start) / time.Millisecond)
}

func Log(message string) {
	fmt.Printf("%vms\t%q\n", Elapsed(), message)
}

func Fetch(url string) string {
	reply, error := http.Get(url)
	if error != nil {
		return ""
	}

	defer reply.Body.Close()

	content, err := ioutil.ReadAll(reply.Body)
	if err != nil {
		return ""
	}

	return string(content)
}

var finished chan int

func InitWorkerPool() {
	finished = make(chan int)
}

func JoinGoroutines(numWorkers int) {
	for i := 0; i < numWorkers; i++ {
		<-finished
	}
}

func LaunchWorker(i int, url string) {
	Fetch(url)
	Log(url)

	finished <- i
}

func main() {
	if len(os.Args) <= 1 {
		os.Exit(0)
	}

	urls := os.Args[1:]
	numWorkers := len(urls)

	InitWorkerPool()
	StartTimer()

	for i, url := range urls {
		// Detach goroutine
		go LaunchWorker(i, url)
	}

	JoinGoroutines(numWorkers)
}
