package tabletest

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestCoverage(t *testing.T) {
	var tests = []struct {
		input string
		want  time.Duration
		err   bool
	}{
		{"500ns", 500 * time.Nanosecond, false},
		{"1ns", 1 * time.Nanosecond, false},
		{"+-1ns", 1 * time.Nanosecond, true},
		{"-100ns", -100 * time.Nanosecond, false},
		{"0", 0, false},
		{"", 0, true},
		{"1e", 0, true},
		{"ms", 0, true},
		{"z", 0, true},
		{"1.109m", 1*time.Minute + 6540*time.Millisecond, false},
		{"199999999999.99999h", 0, true},
		{"19.m", 19 * time.Minute, false},
		{"12t32m", 0, true},
		{"1.1t1m", 0, true},
		{"-.s", 0, true},
		{"-....s", 0, true},
		{"-.", 0, true},
		{"1", 0, true},
		{"99999999999999999999999999999999999999999h", 0, true},
		{"1.873462983746298374629387462398746239847m", 1*time.Minute + 52407779024*time.Nanosecond, false},
		{"231231231220000000002s", 0, true},
		{"231231231220000000112s", 0, true},
		{"231231231312312312312s", 0, true},
		{"931231999999999312312s", 0, true},
		{"931231231312312312s", 0, true},
		{"92373643242347583927384959323s", 0, true},
		{".9223372036854775809", 0, true},
	}
	for _, test := range tests {
		v, err := ParseDuration(test.input)

		if test.err {
			assert.NotEqual(t, nil, err, "Didn't catch error")
		} else {
			assert.Equal(t, test.want, v, "Wrong result")
			assert.Equal(t, nil, err, "Unexpected error")
		}
	}
}
